//
//  Observable.swift
//  ViewController-Lifecycle
//
//  Created by Samat Murzaliev on 08.04.2022.
//

import Foundation

class Observable<T> {
    var value: T? {
        didSet{
            listeners.forEach {
                $0!(value)
            }
        }
    }
    
    private var type: T.Type? = nil
    
    init(_ type: T.Type) {
        self.type = type
    }
    
    private var listeners: [((T?) -> Void)?] = []
    
    func subscribe(_ listener: @escaping (T?) -> Void) {
        listener(value)
        self.listeners.append(listener)
    }
}
