//
//  MainViewModel.swift
//  ViewController-Lifecycle
//
//  Created by Samat Murzaliev on 08.04.2022.
//

import Foundation
import UIKit

class MainViewModel {
    var timerObservable = Observable(String.self)
    var countDoun = 0
    var timer = Timer()
    
    func runTimer() {
        
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateView), userInfo: nil, repeats: true)
    }
    
    @objc func updateView() {
        countDoun += 1
        timerObservable.value = "Timer: \(countDoun)"
    }
    
    func stopTimer() {
        timer.invalidate()
    }
    
    func resetTimer() {
        countDoun = 0
    }
}
