//
//  ViewController.swift
//  ViewController-Lifecycle
//
//  Created by Samat Murzaliev on 07.04.2022.
//

import UIKit
import SnapKit

class MainViewController: UIViewController {
    
    let mainViewModel = MainViewModel()
    
    var screenCounter = 0
    
    private lazy var screenCount: UILabel = {
        let view = UILabel()
        view.font = .systemFont(ofSize: 12, weight: .medium)
        view.textColor = .brown
        view.text = "Number of UIViewControllers in memory: 1"
        view.textAlignment = .center
        return view
    }()
    private lazy var timerLabel: UILabel = {
        let view = UILabel()
        view.font = .systemFont(ofSize: 22, weight: .bold)
        view.textColor = .blue
        view.text = "Timer here"
        view.textAlignment = .center
        return view
    }()
    
    private lazy var switchButton: UIButton = {
        let view = UIButton(type: .system)
        view.setTitle("Next Screen", for: .normal)
        view.backgroundColor = .darkGray
        view.setTitleColor(UIColor.white, for: .normal)
        view.addTarget(self, action: #selector(switchPressed(view:)), for: .touchUpInside)
        return view
    }()
    
    private lazy var resetButton: UIButton = {
        let view = UIButton(type: .system)
        view.setTitle("Reset Timer", for: .normal)
        view.backgroundColor = .red
        view.setTitleColor(UIColor.white, for: .normal)
        view.addTarget(self, action: #selector(resetPressed(view:)), for: .touchUpInside)
        return view
    }()
    
    @objc func switchPressed(view: UIButton) {
        navigationController?.pushViewController(SecondaryViewController(), animated: true)
    }
    
    @objc func resetPressed(view: UIButton) {
        mainViewModel.resetTimer()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        mainViewModel.timerObservable.subscribe { label in
            self.timerLabel.text = label
        }
        mainViewModel.runTimer()
        setSubViews()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        mainViewModel.stopTimer()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        mainViewModel.stopTimer()
        mainViewModel.runTimer()
        screenCounter = navigationController?.viewControllers.count ?? 0
        screenCount.text = "Number of UIViewControllers in memory: \(screenCounter)"
    }
    
    private func setSubViews() {
        view.backgroundColor = .white
        
        view.addSubview(timerLabel)
        timerLabel.snp.makeConstraints { make in
            make.height.equalTo(30)
            make.width.equalTo(150)
            make.centerX.equalToSuperview()
            make.top.equalToSuperview().offset(200)
        }
        
        view.addSubview(screenCount)
        screenCount.snp.makeConstraints { make in
            make.height.equalTo(30)
            make.width.equalTo(300)
            make.top.equalTo(timerLabel.snp.bottom).offset(20)
            make.centerX.equalToSuperview()
        }
        
        view.addSubview(switchButton)
        switchButton.snp.makeConstraints { make in
            make.height.equalTo(30)
            make.width.equalTo(100)
            make.centerX.equalToSuperview()
            make.top.equalTo(timerLabel.snp.bottom).offset(60)
        }
        
        view.addSubview(resetButton)
        resetButton.snp.makeConstraints { make in
            make.height.equalTo(30)
            make.width.equalTo(100)
            make.centerX.equalToSuperview()
            make.top.equalTo(switchButton.snp.bottom).offset(20)
        }
    }
}

