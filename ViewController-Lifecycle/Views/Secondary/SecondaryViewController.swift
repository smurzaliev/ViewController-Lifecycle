//
//  SecondaryViewController.swift
//  ViewController-Lifecycle
//
//  Created by Samat Murzaliev on 07.04.2022.
//

import UIKit

class SecondaryViewController: UIViewController {
    
    private lazy var popButton: UIButton = {
        let view = UIButton(type: .system)
        view.setTitle("popViewController", for: .normal)
        view.backgroundColor = .green
        view.setTitleColor(UIColor.white, for: .normal)
        view.addTarget(self, action: #selector(popPressed(view:)), for: .touchUpInside)
        return view
    }()
    
    private lazy var pushButton: UIButton = {
        let view = UIButton(type: .system)
        view.setTitle("pushViewController", for: .normal)
        view.backgroundColor = .red
        view.setTitleColor(UIColor.white, for: .normal)
        view.addTarget(self, action: #selector(pushPressed(view:)), for: .touchUpInside)
        return view
    }()
    
    @objc func popPressed(view: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @objc func pushPressed(view: UIButton) {
        navigationController?.pushViewController(MainViewController(), animated: true)
    }
    
    override func viewDidLoad() {
        setSubViews()
    }
    
    private func setSubViews() {
        view.backgroundColor = .brown
        
        view.addSubview(popButton)
        popButton.snp.makeConstraints { make in
            make.height.equalTo(30)
            make.width.equalTo(200)
            make.centerX.equalToSuperview()
            make.top.equalToSuperview().offset(250)
        }
        
        view.addSubview(pushButton)
        pushButton.snp.makeConstraints { make in
            make.height.equalTo(30)
            make.width.equalTo(200)
            make.centerX.equalToSuperview()
            make.top.equalTo(popButton.snp.bottom).offset(20)
        }
    }
}
